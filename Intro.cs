﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Xunit;

namespace UiAutomationCore
{
    public class Intro : IDisposable
    {
        private readonly IWebDriver _driver;
        public Intro()
        {
            _driver = new ChromeDriver();
        }

        [Fact]
        public void TestExample()
        {
            _driver.Navigate()
                .GoToUrl("https://google.com");

            Assert.Equal("Google", _driver.Title);
            Assert.Contains("google", _driver.PageSource);
        }

        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }
    }
}
